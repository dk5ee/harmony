All hardware considerations

* double sided pcb, less than 10cm x 10cm
* power supply 12V, 9V, 5V
* atmega controller with program for conversion midi to sid
* * with 20Mhz crystal
* * two leds for debugging while developing
* * two DIN connecters with optocoupler for midi
* * 6 pin ISP connector
* 8 bit R2R DAC on atmega
* SID with 1MHz crystal oscillator
* * depending on SID, pcb can be used with 9V or 12V
* * 16 inputs go to atmega
* * three inputs are to be tied to ground
* * two capacitors for filter
* LM386 audio amplifier
