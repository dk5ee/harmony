#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void printall() {
	int i=0;
	printf("const uint8_t sinetable[255] = {\n//only going from 0 to 254 to minimize one rounding error at 127.5 \n");
	for ( i = 0; i <= 255; i++) {
		float f = (float) i ;
		f = f /255.0;
		f = f * M_PI*2;
		f = sin (f);
		f = f + 1.0;
		f = f / 2.0;
		f = f * 254.0;
		int n = round (f);
		printf("\t0x%02x,  // %d  ", n , i);
		printf(" \n");
	}
	printf("};\n");
}




int main(void) {
	printall();
	return EXIT_SUCCESS;
}